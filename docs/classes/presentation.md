### classes[0] = "Presentation"

#### Análise de Sequências Biológicas 2024-2025

![Logo EST](presentation_assets/logo-ESTB.png)

</br>
</br>
</br>

© Francisco Pina Martins 2019-2025

---

### Practical info

* &shy;<!-- .element: class="fragment" --> Schedule: Tuesdays - 14:00 to 16:00; Wednesdays - 14:00 to 16:00 **Room 1.10**
* &shy;<!-- .element: class="fragment" --> Questions? f.pina.martins@estbarreiro.ips.pt
* &shy;<!-- .element: class="fragment" --> [Moodle](https://moodle.ips.pt/2425/course/view.php?id=547)
* &shy;<!-- .element: class="fragment" --> Office hours: Fridays - 12:00 to 14:00

---

### Avaliação

* &shy;<!-- .element: class="fragment" --><font color="orange">Contínua</font> **OU** <font color="deeppink">exame 1ª e/ou 2ª época</font>
* &shy;<!-- .element: class="fragment" --><font color="orange">Avaliação contínua:</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">40% - Contexto sala de aula <font color="red">(3 componentes)</font>:</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">10% - "Homework"</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">25% - "Journal Club"</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">5% - Participação/Atitude/interesse</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">30% - 1º trabalho</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">30% - 2º trabalho</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">Sem notas mínimas</font>
* &shy;<!-- .element: class="fragment" --><font color="deeppink">100% exame</font>

---

### Integridade Académica

* &shy;<!-- .element: class="fragment" -->Plágio
* &shy;<!-- .element: class="fragment" -->Abuso de IA
* &shy;<!-- .element: class="fragment" -->Etc..

&shy;<!-- .element: class="fragment" -->![Skull road](presentation_assets/skull_road.png)

---

### What are "biological sequences"?

* &shy;<!-- .element: class="fragment" -->A biological sequence is a single, continuous molecule of nucleic acid or protein.
* &shy;<!-- .element: class="fragment" -->Sequences can represent multiple types of information

|||

### Such as?

* &shy;<!-- .element: class="fragment" -->By molecule type:
  * &shy;<!-- .element: class="fragment" --><font color="orange">DNA</font>
  * &shy;<!-- .element: class="fragment" --><font color="green">RNA</font>
  * &shy;<!-- .element: class="fragment" --><font color="cyan">Protein</font>
* &shy;<!-- .element: class="fragment" -->By data structure type:
  * &shy;<!-- .element: class="fragment" --><font color="purple">Single sequence</font>
  * &shy;<!-- .element: class="fragment" --><font color="yellow">Multiple sequences</font>
  * &shy;<!-- .element: class="fragment" -->Can be organized as:
    * &shy;<!-- .element: class="fragment" --><font color="#FFCC66">Independent sequences</font>
    * &shy;<!-- .element: class="fragment" --><font color="navy">Alignments</font>
    * &shy;<!-- .element: class="fragment" --><font color="grey">Mappings</font>
    * &shy;<!-- .element: class="fragment" --><font color="deeppink">Assemblies</font>

---

### Sooo... Why do we analyse them?

* Phylogenetic trees <!-- .element: class="fragment" data-fragment-index="1" -->
* Population genetics/genomics <!-- .element: class="fragment" data-fragment-index="2" -->
* Organism identification <!-- .element: class="fragment" data-fragment-index="3" -->
* Natural selection detection <!-- .element: class="fragment" data-fragment-index="4" -->
* Finding gene variants <!-- .element: class="fragment" data-fragment-index="5" -->
* Disease prediction <!-- .element: class="fragment" data-fragment-index="6" -->
* Association studies <!-- .element: class="fragment" data-fragment-index="7" -->
* etc.. <!-- .element: class="fragment" data-fragment-index="8" -->

---

### Overview

* Setting up a working environment <!-- .element: class="fragment" data-fragment-index="1" -->
* Sequence concepts revision <!-- .element: class="fragment" data-fragment-index="2" -->
* Sequence formats <!-- .element: class="fragment" data-fragment-index="3" -->
* Database access <!-- .element: class="fragment" data-fragment-index="4" -->
* Sequence alignments <!-- .element: class="fragment" data-fragment-index="5" -->
* An intro to Phylogenetics <!-- .element: class="fragment" data-fragment-index="6" -->
* High Throughput Sequencing data analyses <!-- .element: class="fragment" data-fragment-index="7" -->

---

### References

* [NCBI Toolbox](https://www.ncbi.nlm.nih.gov/IEB/ToolBox/SDKDOCS/BIOSEQ.HTML)
* [IBS: an illustrator for the presentation and visualization of biological sequences](https://doi.org/10.1093/bioinformatics/btv362)
