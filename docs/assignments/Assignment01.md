# Assignment 01

## *Knee deep into phylogenetics*


### Deadline:

May 15th 2024 at 23:59


### Delivery format:

* English or Portuguese
* PDF or HTML


### Delivery method:

E-mail


### Groups:

* Groups of 4 elements
* Only one group != 4 will be allowed
* Not respecting this rule will **severely** hamper your grade


### Your task:

* Search the bibliography for a research paper that performs phylogenetic analyses;
    * The paper need to be approved for use;
    * read the rules carefully as the number of "tries" counts towards your grade;
* Understand the proposed biological problem;
* Obtain the sequences used in that research paper;
* Reproduce the analyses therein performed using the programs you learned in class (you do not have to use all the methods present in the original paper, only those you deem more important);
* Interpret the obtained phylogenetic trees;
* Compare your results to those of the original paper;
* Make sure your analyses are reproducible;
* Write a report on what you did;


### In detail:

#### Cover

According to EST's rules. Must include *at least*:

* Title
* EST Barreiro logo
* Date
* Student ID
* Curricular Unit name


#### Introduction

A short (1-3 pages) section describing:
  
* The original biological problem;
* The original used methods;
* The conclusions from the original paper;
* The objectives of **your work**;


#### Materials & Methods

A section where you **detail** how your analyses were performed:

* Which methods were used **for each step**;
* Which software (don't forget to include version number **and** paper citation) was used;
* Where can any used scripts be found;


#### Results

Write about what **your** analyses revealed. Report only **observable facts** here.

* Include any phylogenetic trees you deem necessary;
* Describe their most important features;
    * Include both tree topology and support;


#### Discussion

Use this section to interpret the results in a biological context.

* How does the tree help solve the biological issue?
* How does it compare to the one in the original paper?

Optionally, finish with a *conclusion* section if you think it makes sense in your specific case.


#### References

Don't just present a list of consulted material. That simply won't suffice anymore.
For this work (and from now on) you must present the citations in text in addition to the reference list. The used style for this assignment is *APA (American Psychological Association) 7th ed.*, which is quite standard and well supported by reference managing software.

Akihito et al. 2016 uses this style.

**Used software must be referenced using the paper where it was described (if available - but most are).**

*Example from [here](https://doi.org/10.1093/molbev/msae059):* Finally, the species tree was constructed using RAxML-NG (v1.0.2; Kozlov et al. 2019) and replicated 1,000 times using the GTR + G model.

It is highly recommended that you use reference managing software, like [Zotero](https://www.zotero.org/), [Mendeley](https://www.mendeley.com/download-desktop-new/), or [Endnote](https://endnote.com/) (But there are more alternatives).


### Hints:

* Choose a paper that analyses no less than 3 genes;
    * Also, not much more than 6 genes, but we can be more flexible on the upper limit;
    * **No genomes! Not even mitochondrial!**
* To make your analyses **reproducible**, detail them as much as you can (ideally in scripts), so that you can fully repeat them 5 years later;
* This implies **documenting** each and every command and parameter you use;
