# Assignment 02

## *Metagenomics at your fingertips*


### Deadline:

June 27th 2024 at 23:59


### Delivery format:

* English or Portuguese;
* PDF or HTML;


### Delivery method:

* Report document via e-mail;
* Any scripts or whatever alternate automation method you see fit should be placed in an public git repository (like github or gitlab) and the corresponding commit hash should be mentioned in the report and delivery email;


### Groups:

* Groups of 4 elements
* Only one group != 4 will be allowed
* Not respecting this rule will **severely** hamper your grade


### Your Task:

For this assignment you will have to:

* Search the bibliography for a research paper that performs 16S amplicon data analyses;
    * The paper need to be approved for use;
    * Read the rules carefully as the number of "tries" counts towards your grade;
    * You can also look for 18S amplicon (for metazoans), or ITS amplicon for fungi, but it might be slightly harder;
* Understand the presented biological problem;
* Obtain the *demultiplexed* reads used in the original paper;
    * Where to obtain this data should be stated in the paper, usually in the form of accession numbers in the main text;
* Reduce the dataset to something you consider manageable (optional):
    * If your CPU is an "old potato" and you have low amounts of RAM available (≤ 8GB), don't go above 100K reads per sample;
    * If your CPU is "mid-range" and you have a decent amount of RAM available (≤ 16GB), don't go above 300K reads per sample;
    * If you are lucky enough to have a "high-end" CPU (anything above a Ryzen 7 2700) available and have a generous amount of RAM available (≥ 20GB) you can probably analyse the entire dataset!;
        * No need for randomization strategies, you can just use the first *N* reads from the files;
        * *Hint*: `head` is your friend when reducing your dataset (just like in class);
* Reanalyse the (eventually reduced) data using methods learned in class;
    * Compare your results with those in the original paper;
* Make sure your analyses are reproducible;
* Write a report on what you did.


### In detail:

#### Cover

According to EST's rules. Must include *at least*:

* Title
* EST Barreiro logo
* Date
* Student ID
* Curricular Unit name


#### Introduction

A short (1-3 pages) section describing:
  
* The original biological problem;
* The original used bioinformatics pipeline;
    * As a figure;
* The conclusions from the original paper;


#### Objectives

* A single paragraph of context;
* The main goal of your work as a bullet point;
    * Technical tasks needed for your main goal as sub-bullet points;


#### Materials & Methods

A section where you **detail** how your analyses were performed:

* Which methods were used **for each task**;
* Which software (don't forget to include version number) was used;
* Where can the used scripts be found (there is no way to work around it this time. You **must** make the code available);
    * Create a [github](https://github.com) or [gitlab](https://gitlab.com) repository for this;
    * You are not restricted to these two services, but a `git` repository is a requirement;


#### Results

Produce plots equivalent to those in the original paper (at least like the ones from class) and present them here (eventually alongside the original ones).

* Are your plots *compatible* with those from the original paper?
    * This is a simple "yes" or "no" question;
    * Critically analyzing the "why" will help you find any mistakes in the process (it is highly unlikely you succeed at first try, don't feel bad about it, just keep trying);


#### Discussion

Expand on the results' answer here.

* Explore the differences between your results and the original ones;
    * Interpret your results and highlight what is compatible with the original results and what isn't;


#### Author contributions

Note down which tasks each group member was responsible for. Which skills did each member felt they improved?


#### References

* Don't just present a list of consulted material. That simply won't suffice anymore.
* For this work you must present the citations in text in addition to the reference list. This time the mandatory style is "Frontiers in Plant Science".
* See how it is done in [Simmons et al. 2020](https://doi.org/10.3389/fpls.2020.00599).
* It is highly recommended that you use reference managing software, like [Zotero](https://www.zotero.org/), [Mendeley](https://www.mendeley.com/download-desktop-new/), or [Endnote](https://endnote.com/) (But there are other alternatives).
* **Used software must be referenced using the paper where it was described (if available - but most are), not the website where you found it!** 


### Hints:

* The tougher part of this task will be to find a suitable paper with an associated and available dataset. If you thought the last one was hard, just wait until you try this one!
* Make your analyses **reproducible** (detail them as much as you can, so that you can fully repeat them 5 years later);
* Include any commands you use;
* Any used scripts (even if they are only for keeping track of commands), **must** be placed in an online repository like [github](https://github.com) or [gitlab](https://gitlab.com);
* This type of data might take anywhere from several minutes to 24h to analyse (depending on CPU speed and dataset size). **Plan in advance**;
* Did I mention references before? **DO NOT FORGET TO INCLUDE REFERENCES!!** These have, of course, to be included in the main text, like you see in every research paper;
    * Do not cite only the paper you tried to reproduce. Knowledge comes from other papers too. Cite them as you gather information from them;
* This analysis is not as straightforward as the ones from the tutorials. **There are bound to be problems and other analyses issues**. Solve them as best you can;
* Do not hesitate to ask any questions you may have via email;


## Be at your 'top game' for this one. I'm counting on you.
