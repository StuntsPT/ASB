# Population Structure: Parallelized *STRUCTURE* and *STRUCTURE* like analyses on SNP datasets

This class will walk you through to obtaining *admixture* plots from a SNP dataset obtained through *ipyrad* (Ok, not just *ipyrad*, but rather any VCF file).
Inferring population structure is a frequently performed analysis, to such an extent that a lot of methods and implementations exist to perform it. Most, however are based on the original [Pritchard et al. 2000](https://www.genetics.org/content/155/2/945?ijkey=0dce2e21de8a777a7123815a3222fcfc0f35df3d&keytype2=tf_ipsecsha) method, which is still one of the most complete implementations. Since this is one of the slowest implementations, however, we will focus this tutorial on other, faster, approaches.


### Estimating 'K'

Identifying the true number of genetic clusters in a sample is a long standing, and difficult problem (see for example [Evanno et al 2005](https://onlinelibrary.wiley.com/doi/full/10.1111/j.1365-294X.2005.02553.x), [Verity & Nichols 2016](http://www.genetics.org/content/early/2016/06/10/genetics.115.180992), and particularly [Janes et al 2017 (titled "The K = 2 conundrum")](https://onlinelibrary.wiley.com/doi/abs/10.1111/mec.14187)). Famously, the method of identifying the best K presented in [Pritchard et al (2000)](http://www.genetics.org/content/155/2/945) is described as "dubious at best". Even Evanno et al (2005) (cited over 12,000 times) "... insist that this (deltaK) criterion is another ad hoc criterion...." Because of this we stress that population structure analysis is as an exploratory method, and should be approached in a hierarchical fashion. Although methods such as *STRUCTURE* and *fastStrcuture* have a "standard" way to estimate `K`, programs like *MavericK* are focused precisely on improved estimations of this value, and *ALStructure* has no way to estimate it when missing data is present.


## Setup

In order to perform *STRUCTURE* like analyses in a **fast**, **automated** and **reproducible** way we will setup and use [*Structure_threader*](https://structure-threader.readthedocs.io/en/latest/). This software wraps four different programs that perform STRUCTURE analyses under an unifying interface and performs multiple runs in parallel. The 4 programs *Structure_threader* can wrap and automate are:

* [*STRUCTURE*](https://web.stanford.edu/group/pritchardlab/structure.html) - [Paper](https://www.genetics.org/content/155/2/945)
* [*fastStructure*](http://rajanil.github.io/fastStructure/) - [Paper]( https://doi.org/10.1534/genetics.114.164350)
* [*MavericK*](https://github.com/bobverity/maverick) - [Paper](https://dx.doi.org/10.1534%2Fgenetics.115.180992)
* [*ALStructure*](https://github.com/StoreyLab/alstructure) - [Paper](https://doi.org/10.1534/genetics.119.302159)

Under GNU/Linux and OSX systems, *Structure_threader* will automatically install all of the "wrappable" programs for you. Under Windows, well, you are on your own. 


### Installing *Structure_threader*

We can now use `conda` to create a new environment and install *Structure_threader* in that environment. By now I really shouldn't have to show you how, but here it is anyway:

```bash
# Install miniconda (You don't have to do this again if it is already working)
$ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
$ bash Miniconda3-latest-Linux-x86_64.sh -b -f  # This is new!
$ ~/miniconda3/bin/conda init bash  # ZSH users beware
$ exit
```

Open your shell again and install `structure_threader`. The `conda create` command might take a while, so please be patient.

```bash
$ conda config --add channels conda-forge
$ conda create -n structure python=3.9 r r-rcolorbrewer bioconductor-snprelate bioconductor-lfa r-devtools -c bioconda # Create a new environment with all required dependencies
$ conda activate structure  # Activate the new environement
$ python3 -m pip install structure_threader  # Structure_threader does not have a conda package, so we install it via pip, python's package manager
```

These steps should get you *Structure_threader* installed and running. In order to make sure everything is in working order, you should enter `structure_threader` in your CLI, and you should get an output similar to the one below:

```
usage: Structure_threader [-h] {run,plot,params} ...

A software wrapper to paralelize genetic clustering programs.

positional arguments:
  {run,plot,params}  Select which structure_threader command you wish to execute.
    run              Performs a complete run of structure_threader.
    plot             Performs only the plotting operations.
    params           Generates mainparams and extraparams files.

optional arguments:
  -h, --help         show this help message and exit

```


### Getting data

Before we start using *Strucutre_threader* we need to get some data. For this example we will resort to data from the *excellent* [Silliman (2019)](https://doi.org/10.1111/eva.12766) paper. Create a new directory and download the necessary files there:

```bash
$ mkdir ~/str_analyses
$ cd ~/str_analyses
$ wget https://gitlab.com/StuntsPT/asb2023/-/raw/master/docs/assignments/Assignment02.5_assets/oyster.vcf.gz
$ wget https://gitlab.com/StuntsPT/asb2023/-/raw/master/docs/assignments/Assignment02.5_assets/oyster.indfile
```

Since the paper is about *Ostrea lurida*, we just called the data *oyster*. But now would be a really good chance to take a look at the paper, and especially its plots. No really, I'll wait.


### Minimum Minor Allele Frequency (WARNING: controversial issue)

It has been shown that singletons can have a confounding effect ([Linck & Battey 2019](https://doi.org/10.1111/1755-0998.12995)) when inferring population genetic structure. Depending on each case, removing SNPs whose minor allele has a frequency below a certain threshold may be helpful. We will perform this filtering using the program `vcftools`.

```bash
$ conda install -c bioconda vcftools
$ vcftools --gzvcf oyster.vcf.gz --maf 0.005 --max-missing 0.60 --recode --out oysterMAF005MM60
$ mv oysterMAF005MM60.recode.vcf oysterMAF005MM60.vcf
```

Please notice a few important things: 
* The value of 0.005 represents a single allele in 117 diploid individuals (117 * 2 * 0.005 = 1.17, which is < 2 but > 1);
* In the original publication, the authors use a threshold of 0.025 (which is a bit too high, **in my opinion**, representing up to 5 alleles);
* The output from `vcftools` will be named `oysterMAF005MM60.recode.vcf`, which we then rename for clarity;


### Dealing with Linkage Disequilibrium

When analysing RAD-Seq data, linkage disequilibrium (LD) can be a problem [Hendricks et al. 2018](https://onlinelibrary.wiley.com/doi/10.1111/eva.12659). One way to minimize its effect is to use only a single SNP from each locus. To do that we will need yet another script: [vcv_parser.py](https://raw.githubusercontent.com/CoBiG2/RAD_Tools/6648d1ce1bc1e4c2d2e4256abdefdf53dc079b8c/vcf_parser.py), which can be found in [this github repository](https://github.com/CoBiG2/RAD_Tools). Use `wget` to obtain it and perform the filtering:

```bash
$ wget https://raw.githubusercontent.com/CoBiG2/RAD_Tools/6648d1ce1bc1e4c2d2e4256abdefdf53dc079b8c/vcf_parser.py
$ python3 vcf_parser.py --center-snp -vcf oysterMAF005MM60.vcf
```

This command will output a new VCF file, with a smaller number of SNPs than the original called `oysterMAF005MM60CenterSNP.vcf`. You can instead pass `--one-snp` to retain the first SNP instead of the center one, or `--random-snp` to retain a random SNP from each locus.

Just for the sake of completeness, let me state that this step is **not performed** in the original paper.


### Using *Structure_threader*

*Structure_threader*'s CLI interface was built in order to be as simple as possible, considering it has to allow the user to run 4 different programs under a similar interface.
*Structure_threader* can run in three different modes: `run` (to actually run the analyses), `plot` (to draw admixture plots from already performed analyses) and `params` (the generate skeleton parameter files for *STRUCTURE* and *MavericK*). You can ask *Structure_threader* for help on each of the modes by running `structure_threader <mode> -h`. Due to time constraints we will focus on the `run` mode. If you are ever stuck on something, you can always consult [the manual](https://structure-threader.readthedocs.io/en/latest/).
Should you find any bugs on *Structure_threader* you can just rant about them to the author. Go ahead, I'm listening. =-)

For this module we will perform an example run using *ALStructure*. I choose this one for several reasons:

1. It is **very** fast;
2. Its results are similar to those of STRUCTURE;
3. It is based on an interesting "model free" approach;

*ALStrucutre* takes a `.tsv` file as input, which is different from every other software for the same purpose. However, instead of forcing you to convert to another file format, *Structure_threader* will internally convert any `VCF` file to something *ALStructure* can read, so we already have everything we need to get our admixture plot.

```bash
$ structure_threader run -i oysterMAF005MM60CenterSNP.vcf -o ./results_oysterMAF005MM60CenterSNP -als ~/miniconda3/envs/structure/bin/alstructure_wrapper.R -K 10 -t 3 --ind oyster.indfile
```

The first time you run this command, *Structure_threader* will find and install any *ALStructure* R dependencies that may be missing. Since *ALStrucutre* requires **a lot** of dependencies, this *might* take a while. Any subsequent times you run *ALStructure* it will be almost instant.

In the meantime, let's try to digest the huge command we just entered:

* `-i` determines the input file;
* `-o` determines the output directory;
* `-als` determines that we are wrapping *ALStructure* and where the respective `R` script is located;
* `-K` determines the numbers of K we want to test (from 1 to K);
* `-t` determines how many CPU cores we want to use;
* `--ind` determines where our file with individual information is located; let's take a closer look at it (you can do it in another terminal while *ALStructure*'s dependencies are installed);


### The "indfile"

Open `oyster.indfile` with your favourite text editor in a new terminal window (or just use a GUI based text editor, it's really your choice). The first few lines of this file should look like this:

```
BC1_10_C6	Victoria_BC	4
BC1_20_C6	Victoria_BC	4
BC1_22_C7	Victoria_BC	4
BC1_4_C3	Victoria_BC	4
BC1_7_C5	Victoria_BC	4
BC1_8_C4	Victoria_BC	4
BC1_9_C5	Victoria_BC	4
BC2_10_C5	Klaskino_BC	1
```

This file's layout is very important. It lets *Structure_threader* know to which population (column 2) each individual (column 1) belongs to, and in which order (column 3) that population should be placed in the admixture plot. Today this file was provided for you, but normally you will spend some time building it. If/when you have to, just remember you don't have to do all of it manually - the shell tools will always be there for you, just like Sam stood for Frodo.


### Looking at plots

By now, *Structure_threader* should be finished. If we were using STRUCTURE to analyse this dataset (more than 12K SNPs) on these machines, we would be looking towards at least 1-2 **weeks** of runtime. Think about this for a second, to appreciate the difference.

Now use the file browser to navigate to `~/my_structure_results/results_oysterMAF005MM60CenterSNP/plots`. Once there, double-click the file named `alstr_K6.svg` and `alstr_K2.svg`. They should open in any image viewer. You can compare them to those of the [original paper](https://onlinelibrary.wiley.com/action/downloadFigures?id=eva12766-fig-0002&doi=10.1111%2Feva.12766). These plots can be readily used in publications. No need to worry about resolution, since they are in vectorial format which means "infinite" resolution.

But this is not the last "surprise" *Structure_threader* has up its sleeve. Try to double-click the file `alstr_K6.html`. A new browser tab should open, and you can dynamically explore your plot (if you hoover the cursor over an individual bar, you will be presented with more information). Finally, try to double-click the file `ComparativePlot_2-3-4-5-6-7-8-9-10.html`.


### Comparison

When comparing *ALStructure* plots with the ones from the original paper, please bear in mind that there are several methodological differences at play:

* Different filtering methods;
* All loci Vs. outlier/neutral loci;
* STRUCTURE Vs. *ALStructure*;

If you want to make a closer comparison, you can get the VCF files used in the original paper from [Dryad](https://doi.org/10.5061/dryad.114j8m1). Feel free to try this to your heart's contempt.


### What about different methods?

Due to time constraints, we cannot possibly analyse a large dataset, such as the one from Silliman *et al.* (2019) in this class using parametric methods. But I do have 2 smaller datasets you can analyse with *fastStructure* and STRUCTURE (we will skip *MavericK*).

### *fastStructure*

Download [this](https://gitlab.com/StuntsPT/asb2023/-/raw/master/docs/assignments/Assignment02.5_assets/medium_dataset.tar.xz) medium sized dataset (726 SNPs, 33 individuals) to a directory of your choice. Then uncompress the file to reveal it's contents (a *fastStructure* ready input file, `N060j.str` and the respective *indfile*, `N060j.indfile`).
Next, navigate your shell to the directory where you uncompressed these files, and run *Structure_threader* (this timme wrapping *fastStructure*):

```bash
structure_threader run -i N060j.str -fs ~/miniconda3/envs/structure/bin/fastStructure -K 6 -o ./N060j_results -t 3 --ind N060j.indfile
```

You already know what most of these switches do from the previous run. The different ones are:

* `-fs` - The location of the *fastStructure* binary
* `-t` - The number of CPU cores to use

Please do not use more cores than what your machine has available, as doing that will simply make things go more slowly.
Normally you would have to worry about a few more parameters, but for this course's purpose this basic run will suffice.
Finally, take a look at the plots. Note that unlike *ALStructure*, *fastStructure* has a method to help us choose the best value of `K` (number of clusters). *Structure_threader* will digest all of this for you automatically - you just have to read the result from the `N060j_results/bestK/chooseK.txt` file.


### STRUCTURE

In order to run the **very** slow STRUCTURE we need a [really small dataset](https://gitlab.com/StuntsPT/asb2023/-/raw/master/docs/assignments/Assignment02.5_assets/reduced_dataset.tar.xz) (29 SNPs, 34 individuals). Create a new directory, download the file, uncompress it and run the following command:

```bash
structure_threader run -i Reduced_dataset.structure -t 3 -K 3 -R 5 -o ./Reduced_results -st ~/miniconda3/envs/structure/bin/structure --params ./mainparams
```

The new options are:

* `-R` - The number of replicates (STRUCTURE requires run replicates)
* `--params` - The location of the parameter files

STRUCTURE requires **a lot** of parameters to run. So many it would be impractical to pass them all via a command line. As such, STRUCTURE will read 2 files to get all of its parameters: `mainparams` and `extraparams`. Normally you will have to take deep look at STRUCTURE's manual to fill these. But in this course they will be provided for you. You're welcome. =-)

I'd normally tell you to look at the BestK estimates, and the resulting plots, but in this case, they are absolutely meaningless, due to how small the dataset is. You can still look if you are curious, but keep this information in mind.


### Added bonus

We can further explore the data with - you guessed it: a PCA!
For that, just use the knowledge you now posses to build one. Use the files 

```
oyster.indfile
oysterMAF005MM60CenterSNP.vcf
```

PS - Yes, the PCA will look quite different from the plots in the paper. Look at what the authors have plotted and try to figure out why this happens. \_-)

